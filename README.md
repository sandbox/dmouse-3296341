# Fluent

## Install

`composer req drupal/fluent`

## Single value fields.

```php
using($node)->value('id');
using($node)->value('nid');
using($node)->value('toUrl');
```

## Multi Value fields
```php
using($paragraph)->values([
  'field_media',
  'field_term.label',
  'field_media.url',
]);
```
