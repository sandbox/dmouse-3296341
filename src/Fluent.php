<?php

namespace Drupal\fluent_demo;

use Drupal\Core\Entity\EntityBase;
use Drupal\options\Plugin\Field\FieldType\ListItemBase;

/**
 * Magic class to solve field accessing.
 */
class Fluent {

  /**
   * Entity.
   *
   * @var mixed
   */
  private $entity;

  /**
   * Field item resolver.
   *
   * @var \Drupal\fluent\FieldItemResolver
   */
  private FieldItemResolver $fieldItemResolver;

  /**
   * Field resolver.
   *
   * @var \Drupal\fluent\FieldResolver
   */
  private FieldResolver $fieldResolver;

  /**
   * Constructor.
   */
  public function __construct(FieldResolver $fieldResolver, FieldItemResolver $fieldItemResolver) {
    $this->fieldResolver = $fieldResolver;
    $this->fieldItemResolver = $fieldItemResolver;
  }

  /**
   * Set the current entity.
   *
   * @param mixed $entity
   *   Fieldable entity.
   */
  public function using($entity): Fluent {
    $this->entity = $entity;

    return $this;
  }

  /**
   * Obtain the first value in the field.
   *
   * @param string $path
   *   Use dot notation to get the field value.
   * @param mixed $defaultValue
   *   The default value if field is NULL.
   *
   * @return mixed
   *   The field value or default.
   */
  public function value(string $path, $defaultValue = NULL) {
    $resolution = $this->fieldResolution($path, $this->fieldControl(0, FALSE), $defaultValue);
    return $resolution->getValue();
  }

  /**
   * Get the first value in a multi value field.
   *
   * @param string $path
   *   Use dot notation to get the field value.
   * @param mixed|null $defaultValue
   *   The default value if field is NULL.
   *
   * @return \Drupal\fluent\Collection
   *   Collection of values.
   */
  public function firstValue(string $path, $defaultValue = NULL) {
    return $this->nthValue($path, 0, $defaultValue);
  }

  /**
   * Get all values in field.
   *
   * @param string|array $path
   *   Use dot notation to get the field value or array of paths for multiples.
   *
   * @return \Drupal\fluent\Collection
   *   Collection of values.
   */
  public function values(string|array $path): Collection {
    if (is_array($path)) {
      $fields = Collection::make($path);

      return $fields->mapWithKeys(function (string $path, string $alias) {
        $fieldValue = $this->nthValue($path, -1);

        if ($fieldValue instanceof Collection) {
          $value = $fieldValue->all();
        }
        else {
          $value = $fieldValue;
        }

        $rename = is_numeric($alias) ? $path : $alias;

        return [
          $rename => $value,
        ];
      });
    }
    else {
      return $this->nthValue($path, -1);
    }
  }

  /**
   * Return the NTH position in the field.
   *
   * @param string $path
   *   Use dot notation to get the field value.
   * @param int $position
   *   Element possition.
   * @param mixed|null $defaultValue
   *   Default value.
   *
   * @return \Drupal\fluent\Collection
   *   Collection of values.
   */
  public function nthValue(string $path, int $position, $defaultValue = NULL) {
    $isMultivalued = FALSE;
    if ($position === -1) {
      $isMultivalued = TRUE;
    }

    $fieldControl = $this->fieldControl($position, $isMultivalued);
    $resolution = $this->fieldResolution($path, $fieldControl, $defaultValue);

    $field = $resolution->getValue();

    if ($field instanceof EntityBase) {
      $collection = [$field];
    }
    else {
      $collection = $field;
    }

    if (is_array($collection)) {
      $fluentCollection = Collection::make($collection);

      // Flatten values if is a list.
      try {
        $class = $resolution->getFieldDefinition()->getItemDefinition()->getClass();
        $fieldClass = new \ReflectionClass($class);
        if ($fieldClass->isSubclassOf(ListItemBase::class)) {
          return $fluentCollection->mapWithKeys(function ($value) {
            return $value;
          });
        }
      }
      catch (\Exception $e) {
      }

      return $fluentCollection;
    }
    else {
      return $collection;
    }
  }

  /**
   * Resolve field value.
   */
  private function fieldResolution($path, FieldControl $fieldControl, $defaultValue) {
    $resolution = $this->fieldResolver->resolve($this->entity, $this->getPath($path), $fieldControl);
    return $this->fieldItemResolver->resolve($resolution, $this->getPath($path), $defaultValue);
  }

  /**
   * Convert path to array.
   */
  private function getPath(string $path): array {
    $path = preg_replace('/[^a-zA-Z\d._]/', '', $path);
    return explode('.', $path);
  }

  /**
   * Object with the information to control the field accessing.
   */
  private function fieldControl($position, bool $isMultivalued): FieldControl {
    $fieldControl = new FieldControl();
    $fieldControl->setMultivalued($isMultivalued);
    $fieldControl->setPosition($position);

    return $fieldControl;
  }

}
