<?php

namespace Drupal\fluent_demo;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * State class to save each resolution.
 */
class FieldResolution {

  /**
   * Current field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  private FieldDefinitionInterface $fieldDefinition;

  /**
   * Current field.
   *
   * @var mixed
   */
  private $field;

  /**
   * Current value.
   *
   * @var mixed
   */
  private $value;

  /**
   * Parent of current field.
   *
   * @var mixed
   */
  private $parent;

  /**
   * Current value.
   *
   * @return mixed
   *   Current value.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Set current value.
   *
   * @param mixed $value
   *   Field value.
   */
  public function setValue($value): FieldResolution {
    $this->value = $value;
    return $this;
  }

  /**
   * Get current parent.
   *
   * @return mixed
   *   Parent.
   */
  public function getParent() {
    return $this->parent;
  }

  /**
   * Set parent.
   *
   * @param mixed $parent
   *   Parent.
   */
  public function setParent($parent): FieldResolution {
    $this->parent = $parent;
    return $this;
  }

  /**
   * Get field definition.
   *
   * @return mixed
   *   Current field definition.
   */
  public function getFieldDefinition() {
    return $this->fieldDefinition;
  }

  /**
   * Set field definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Field definition.
   */
  public function setFieldDefinition(FieldDefinitionInterface $fieldDefinition): FieldResolution {
    $this->fieldDefinition = $fieldDefinition;
    return $this;
  }

  /**
   * Get field.
   *
   * @return mixed
   *   Current field.
   */
  public function getField() {
    return $this->field;
  }

  /**
   * Set field.
   *
   * @param mixed $field
   *   Current field.
   */
  public function setField($field): FieldResolution {
    $this->field = $field;
    return $this;
  }

  /**
   * Determinate if is a multi value field.
   */
  public function isMultiValue(): bool {
    return (
      $this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == -1
      ||
      $this->fieldDefinition->getFieldStorageDefinition()->getCardinality() > 1
      );
  }

}
