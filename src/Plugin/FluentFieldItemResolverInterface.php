<?php

namespace Drupal\fluent_demo\Plugin;

/**
 * Interface for fluent_field_item_resolver plugins.
 */
interface FluentFieldItemResolverInterface {

  /**
   * Method to determinate if this plugin can cast the field value.
   *
   * @param mixed $field
   *   Current field.
   *
   * @return bool
   *   True is this plugin can handle the field value.
   */
  public function can($field): bool;

  /**
   * Method to cast the field value.
   *
   * @param mixed $field
   *   Current field.
   *
   * @return mixed
   *   Current field value casted.
   */
  public function handler($field);

}
