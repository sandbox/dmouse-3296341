<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;
use Drupal\options\Plugin\Field\FieldType\ListFloatItem;
use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;
use Drupal\options\Plugin\Field\FieldType\ListItemBase;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "list",
 *   label = @Translation("List field"),
 * )
 */
class ListItem extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof ListItemBase;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    $key = $field->get('value')->getValue();

    return [$key => $this->castValue($field)];
  }

  /**
   * Cast the list value to correct one.
   *
   * @param \Drupal\options\Plugin\Field\FieldType\ListItemBase $field
   *   Current field.
   *
   * @return float|int|string
   *   Casted value.
   */
  protected function castValue(ListItemBase $field) {
    $value = $field->get('value')->getValue();

    if ($field instanceof ListFloatItem) {
      return (float) $value;
    }
    elseif ($field instanceof ListIntegerItem) {
      return (int) $value;
    }

    return $value;
  }

}
