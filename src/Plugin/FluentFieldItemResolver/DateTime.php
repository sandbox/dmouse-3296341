<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\Core\TypedData\Plugin\DataType\DateTimeIso8601;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "datetime",
 *   label = @Translation("DateTime field"),
 * )
 */
class DateTime extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof DateTimeItem || $field instanceof DateTimeIso8601;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    if ($field instanceof DateTimeIso8601) {
      $resolvedValue = $field->getDateTime();
    }
    elseif ($field instanceof DateRangeItem) {
      $resolvedValue = [
        'start_date' => $field->start_date,
        'end_date' => $field->end_date,
      ];
    }
    else {
      $resolvedValue = $field->date;
    }

    return $resolvedValue;
  }

}
