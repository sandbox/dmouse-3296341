<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "link",
 *   label = @Translation("Link field"),
 * )
 */
class Link extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof LinkItem;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    return $field->getUrl();
  }

}
