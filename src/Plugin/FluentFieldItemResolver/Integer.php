<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;
use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem as IntegerItemField;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "integer",
 *   label = @Translation("Integer field"),
 * )
 */
class Integer extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof IntegerItemField;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    return (int) $field->getString();
  }

}
