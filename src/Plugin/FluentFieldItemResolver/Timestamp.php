<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Plugin\Field\FieldType\TimestampItem;
use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "timestamp",
 *   label = @Translation("Timestamp field"),
 * )
 */
class Timestamp extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof TimestampItem;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    return DrupalDateTime::createFromTimestamp((int) $field->getString());
  }

}
