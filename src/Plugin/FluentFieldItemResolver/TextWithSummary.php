<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;
use Drupal\text\Plugin\Field\FieldType\TextWithSummaryItem;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "text_with_summary",
 *   label = @Translation("Text with summary"),
 * )
 */
class TextWithSummary extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof TextWithSummaryItem;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    return $field->toArray();
  }
}
