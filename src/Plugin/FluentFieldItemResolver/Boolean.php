<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldItemResolver;

use Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem;
use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginBase;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "boolean",
 *   label = @Translation("Boolean field"),
 * )
 */
class Boolean extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof BooleanItem;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field) {
    return $field->getString() == 1;
  }

}
