<?php

namespace Drupal\fluent_demo\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\fluent_demo\Annotation\FluentFieldItemResolver;

/**
 * FluentFieldItemResolver plugin manager.
 */
class FluentFieldItemResolverPluginManager extends DefaultPluginManager {

  /**
   * Constructs FluentFieldItemResolverPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
          'Plugin/FluentFieldItemResolver',
          $namespaces,
          $module_handler,
          FluentFieldItemResolverInterface::class,
          FluentFieldItemResolver::class
      );

    $this->alterInfo('fluent_field_item_resolver_info');
    $this->setCacheBackend($cache_backend, 'fluent_field_item_resolver_plugins');
  }

}
