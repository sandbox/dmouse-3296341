<?php

namespace Drupal\fluent_demo\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\fluent_demo\Annotation\FluentFieldResolver;

/**
 * Provides the Fluent field resolver plugin manager.
 */
class FluentFieldResolverManager extends DefaultPluginManager {

  /**
   * Constructs a new FluentFieldResolverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
          'Plugin/FluentFieldResolver',
          $namespaces,
          $module_handler,
          FluentFieldResolverInterface::class,
          FluentFieldResolver::class
      );

    $this->alterInfo('fluent_fluent_field_resolver_info');
    $this->setCacheBackend($cache_backend, 'fluent_fluent_field_resolver_plugins');
  }

}
