<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldResolver;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\fluent_demo\Exception\NotFieldFoundException;
use Drupal\fluent_demo\FieldControl;
use Drupal\fluent_demo\Plugin\FluentFieldResolverBase;

/**
 * Common field resolver.
 *
 * @FluentFieldResolver(
 *   id = "common_field",
 *   label = "Commond Field Resolver",
 *   weight = 100
 * )
 */
class CommonField extends FluentFieldResolverBase {

  /**
   * {@inheritdoc}
   */
  public function handler($object, $fieldName, FieldControl $fieldControl) {
    if ($object instanceof FieldItemListInterface) {
      $cardinality = $object->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();

      $items = [];
      foreach ($object as $item) {
        $items[] = $item;
      }

      if ($fieldControl->isMultivalued() && ($cardinality == -1 | $cardinality > 1)) {
        return $items;
      }

      return $object->first();
    }

    // @todo refactor this code.
    // First try to access as a field
    // if not exist try to access as a method
    $field = NULL;
    try {
      $field = $object->get($fieldName);
    }
    catch (\InvalidArgumentException $e) {
      if ($method = $this->getMethod($object, $fieldName)) {
        $field = $object->{$method}();
      }

      if ($field === NULL) {
        // @todo Error handling.
        $fields = array_keys($object->getFields());

        // Remove __ methods.
        $methods = get_class_methods($object);
        $methods = preg_replace('/__./', NULL, $methods);

        $message = sprintf(
          'Available fields: %s

      Available methods: %s',
          implode(', ', $fields),
          implode(', ', $methods)
        );

        throw new NotFieldFoundException($message);
      }
    }

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool {
    return TRUE;
  }

  /**
   * Check if field exist.
   *
   * @param mixed $object
   *   Current object.
   * @param mixed $fieldName
   *   Current field name.
   *
   * @return bool
   *   True if exist.
   */
  public function hasField($object, $fieldName) {
    return method_exists($object, 'hasField') && $object->hasField($fieldName);
  }

  /**
   * Check if method exist.
   *
   * @param mixed $object
   *   Current object.
   * @param mixed $possibleName
   *   Current field name.
   *
   * @return string|null
   *   Name or null.
   */
  public function getMethod($object, $possibleName) {
    if ($this->isMethod($object, $possibleName)) {
      return $possibleName;
    }

    $prefixes = [
      'get',
      'is',
      'to',
    ];

    foreach ($prefixes as $prefix) {
      $name = $prefix . ucfirst($possibleName);
      if ($this->isMethod($object, $name)) {
        return $name;
      }
    }

    return NULL;
  }

  /**
   * Check if is method.
   *
   * @param mixed $entity
   *   Current entity.
   * @param mixed $name
   *   Current field name.
   *
   * @return bool
   *   True if method exist.
   */
  public function isMethod($entity, $name) {
    return method_exists($entity, $name);
  }

}
