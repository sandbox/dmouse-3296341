<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldResolver;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\fluent_demo\FieldControl;
use Drupal\fluent_demo\Plugin\FluentFieldResolverBase;

/**
 * Load revision fields.
 *
 * @FluentFieldResolver(
 *   id = "revision_field",
 *   label = "Entity Revision Field",
 *   weight = 100
 * )
 */
class RevisionField extends FluentFieldResolverBase {

  /**
   * {@inheritdoc}
   */
  public function handler($object, $fieldName, FieldControl $fieldControl) {

    $storage = $this->entityType
      ->getStorage($object->getFieldDefinition()->getTargetEntityTypeId());

    if ($fieldControl->isMultivalued()) {

      if (!method_exists($storage, 'revisionIds')) {
        throw new \Exception('Don\'t support multiple revisions');
      }

      $revision = $storage->loadMultipleRevisions(
            array_reverse($storage->revisionIds($object->getParent()->getEntity()))
        );
    }
    else {
      $revision = $storage->loadRevision($object->value);
    }

    return $revision;
  }

  /**
   * {@inheritdoc}
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool {
    return (
      ($fieldDefinition->getName() === 'vid' && $fieldDefinition->getTargetEntityTypeId() === 'node')
      ||
      ($fieldDefinition->getName() === 'revision_id' && $fieldDefinition->getTargetEntityTypeId() === 'taxonomy_term')
      );
  }

}
