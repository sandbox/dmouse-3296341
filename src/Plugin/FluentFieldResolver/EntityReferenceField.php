<?php

namespace Drupal\fluent_demo\Plugin\FluentFieldResolver;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\fluent_demo\FieldControl;
use Drupal\fluent_demo\Plugin\FluentFieldResolverBase;

/**
 * Entity reference resolver.
 *
 * @FluentFieldResolver(
 *   id = "entity_reference",
 *   label = "Entity Reference Resolver",
 *   weight = 90
 * )
 */
class EntityReferenceField extends FluentFieldResolverBase {

  /**
   * {@inheritdoc}
   */
  public function handler($object, $fieldName, FieldControl $fieldControl) {

    if ($object instanceof EntityReferenceFieldItemListInterface) {
      $cardinality = $object->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
      $entities = $object->referencedEntities();

      if ($fieldControl->isMultivalued() && ($cardinality == -1 | $cardinality > 1)) {
        return $entities;
      }

      $position = 0;
      if ($fieldControl->getPosition() != -1) {
        $position = $fieldControl->getPosition();
      }

      return $entities[$position];
    }

    if ($object instanceof EntityReferenceItem) {
      return $this->loadEntityUsingFieldDefinition($object);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool {
    $types = [
      'entity_reference',
      'entity_reference_revisions',
      'image',
    ];

    return in_array($fieldDefinition->getType(), $types);
  }

  /**
   * Resolve entities loading automatically.
   */
  public function loadEntityUsingFieldDefinition($object) {
    $entityType = $object->getFieldDefinition()->getSetting('target_type');

    return $this->entityType->getStorage($entityType)
      ->load($object->target_id);
  }

}
