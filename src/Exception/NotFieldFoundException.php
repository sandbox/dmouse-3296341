<?php

namespace Drupal\fluent_demo\Exception;

/**
 * Custom exception to handle field not found.
 */
class NotFieldFoundException extends \Exception {
}
