<?php

namespace Drupal\fluent_demo;

use Illuminate\Support\Collection as BaseCollection;

/**
 * Customize method in Laravel Collection to be compatibles with Drupal.
 */
class Collection extends BaseCollection {}
