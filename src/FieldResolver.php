<?php

namespace Drupal\fluent_demo;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\fluent_demo\Plugin\FluentFieldResolverManager;

/**
 * Allow to solve the field path to get the field item.
 */
class FieldResolver {

  /**
   * List of plugins.
   *
   * @var array
   */
  private array $fieldResolvers = [];

  /**
   * Current entity.
   *
   * @var mixed
   */
  private $entity;

  /**
   * Field resolution save the current state of the path.
   *
   * @var \Drupal\fluent\FieldResolution
   */
  private FieldResolution $resolution;

  /**
   * Constructor.
   */
  public function __construct(FluentFieldResolverManager $plugin) {
    $this->pluginManager = $plugin;

    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      $this->fieldResolvers[$id] = $this->pluginManager->createInstance($id);
    }

    $this->commonResolver = $this->fieldResolvers['common_field'];
    unset($this->fieldResolvers['common_field']);
  }

  /**
   * Main method to resolve field.
   */
  public function resolve($entity, array $path, FieldControl $fieldControl) {
    $this->entity = $entity;
    $this->resolution = new FieldResolution();

    // Save the current entity.
    $tmpEntity = $this->entity;
    $items = count($path);
    $count = 0;

    do {
      $possibleField = $path[$count];

      $solvedField = $this->getResolver($tmpEntity, $possibleField, $fieldControl);

      if ($solvedField === NULL) {
        break;
      }

      $tmpEntity = $solvedField;

      if ($this->skipCount($solvedField)) {
        continue;
      }

      $count++;
    } while ($count < $items);

    return $this->resolution;
  }

  /**
   * If is one instance of those objects skip the count and resolve over there.
   */
  private function skipCount($solvedField): bool {
    return (
      $solvedField instanceof FieldItemListInterface
      ||
      $solvedField instanceof EntityReferenceItem
    );
  }

  /**
   * Obtain the field resolved by the PluginFieldResolver.
   */
  private function getResolver($entity, $fieldName, FieldControl $fieldControl) {
    if (is_array($entity)) {
      $field = [];
      foreach ($entity as $item) {
        $field[] = $this->getResolver($item, $fieldName, $fieldControl);
      }
      $this->resolution->setField($field);
      return $field;
    }

    if (method_exists($entity, 'getFieldDefinition')) {
      $fieldDefinition = $entity->getFieldDefinition($fieldName);

      if ($fieldDefinition) {
        $this->resolution->setFieldDefinition($fieldDefinition);

        foreach ($this->fieldResolvers as $resolver) {
          if ($resolver->can($fieldDefinition)) {
            $field = $resolver->handler($entity->get($fieldName), $fieldName, $fieldControl);
            $this->resolution->setField($field);

            return $field;
          }
        }
      }

      $field = $this->commonResolver->handler($entity, $fieldName, $fieldControl);
      $this->resolution->setField($field);

      return $field;
    }
    else {
      // @todo don't fieldable entity
    }
  }

}
