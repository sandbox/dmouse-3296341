<?php

namespace Drupal\fluent_demo;

/**
 * State class to save option in order to resolve the field.
 */
class FieldControl {

  /**
   * Is multivalue.
   *
   * @var bool
   */
  private bool $multivalued;

  /**
   * Item position.
   *
   * @var string|int
   */
  private $position;

  /**
   * Is multivalue.
   *
   * @return bool
   *   Multi value.
   */
  public function isMultivalued(): bool {
    return $this->multivalued;
  }

  /**
   * Set if user is requesting multi value field.
   *
   * @param bool $multivalued
   *   Is multi value.
   */
  public function setMultivalued(bool $multivalued): void {
    $this->multivalued = $multivalued;
  }

  /**
   * Get position number.
   *
   * @return int|string
   *   Position number.
   */
  public function getPosition() {
    return $this->position;
  }

  /**
   * Set position.
   *
   * @param int|string $position
   *   Position to obtain.
   */
  public function setPosition($position): void {
    $this->position = $position;
  }

}
