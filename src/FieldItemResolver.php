<?php

namespace Drupal\fluent_demo;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\PrimitiveInterface;
use Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginManager;

/**
 * Once get the field item resolve the value inside.
 */
class FieldItemResolver {

  /**
   * Plugin manager.
   *
   * @var \Drupal\fluent_demo\Plugin\FluentFieldItemResolverPluginManager
   */
  private FluentFieldItemResolverPluginManager $pluginManager;

  /**
   * List of plugins.
   *
   * @var array
   */
  private array $fieldResolvers = [];

  /**
   * Constructor.
   */
  public function __construct(FluentFieldItemResolverPluginManager $plugin) {
    $this->pluginManager = $plugin;

    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      $this->fieldResolvers[$id] = $this->pluginManager->createInstance($id);
    }
  }

  /**
   * Main method to resolve field item.
   */
  public function resolve(FieldResolution $resolution, array $path, $defaultValue = NULL) {
    $field = $resolution->getField();

    if (is_array($field)) {
      $value = [];
      foreach ($field as $item) {
        $value[] = $this->getValueField($item, $path, $defaultValue);
      }
    }
    else {
      $value = $this->getValueField($field, $path, $defaultValue);
    }
    $resolution->setValue($value);

    return $resolution;
  }

  /**
   * Resolve the value within field item.
   */
  private function getValueField($field, $path, $defaultValue) {
    if ($field instanceof FieldItemListInterface && $field->isEmpty()) {
      $default = $field->getFieldDefinition()->getDefaultValueLiteral();
      $end = end($path);

      if (!empty($default) && array_key_exists($end, $default[0])) {
        return $default[0][$end];
      }

      return $defaultValue;
    }

    $resolvedValue = $this->resolveFieldValue($field, $defaultValue);

    return $resolvedValue;
  }

  /**
   * Determinate what plugin resolve the field item.
   */
  private function resolveFieldValue($field, $defaultValue) {
    foreach ($this->fieldResolvers as $resolver) {
      if ($resolver->can($field)) {
        return $resolver->handler($field);
      }
    }

    if ($field instanceof PrimitiveInterface || $field instanceof ComplexDataInterface) {
      if ($field->getValue() === NULL || $field->getValue() === '') {
        $value = $defaultValue;
      }
      elseif ($field instanceof ComplexDataInterface) {
        $value = $field->toArray();
      }
      else {
        $value = $field->getString();
      }

      // If value is the unique key in the resolved value we extract and return
      // as unique value, if not like complex fields we return all values.
      if (is_array($value) && count($value) === 1 && array_key_exists('value', $value)) {
        $value = $value['value'];
      }

      return $value ?? $defaultValue;
    }
    return $field ?? $defaultValue;
  }

}
