<?php

namespace Drupal\fluent_demo\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines fluent_field_item_resolver annotation object.
 *
 * @Annotation
 */
class FluentFieldItemResolver extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
