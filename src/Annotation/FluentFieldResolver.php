<?php

namespace Drupal\fluent_demo\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Fluent field resolver item annotation object.
 *
 * @see \Drupal\fluent\Plugin\FluentFieldResolverManager
 * @see plugin_api
 *
 * @Annotation
 */
class FluentFieldResolver extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Weight to sort plugins.
   *
   * @var int
   */
  public $weight;

}
