<?php

namespace Drupal\Tests\fluent_demo\Functional;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Test basic functionality of My Module.
 *
 * @group fluent
 */
class SingleValueTestCase extends FluentTestBase {

  /**
   * Test base node fields.
   */
  public function testBaseNodeField() {
    $node = $this->getNode();
    $title = $node->getTitle();

    $this->assertEquals($title, using($node)->value('title'));
    $this->assertEquals('en', using($node)->value('langcode'));

    $this->assertIsInt(using($node)->value('nid'));
    $this->assertIsString(using($node)->value('id'));
    $this->assertInstanceOf(DrupalDateTime::class, using($node)->value('changed'));
    $this->assertInstanceOf(DrupalDateTime::class, using($node)->value('created'));
  }

  /**
   * Test created field.
   */
  public function testCreatedField() {
    $image = $this->getMediaImage();
    $timestamp = $image->getCreatedTime();
    $ddateTime = fluent()->using($image)->value('created');

    $this->assertInstanceOf(DrupalDateTime::class, $ddateTime);
    $this->assertEquals($ddateTime->getTimestamp(), $timestamp);
  }

  /**
   * Test field_boolean_single.
   */
  public function testFieldBooleanSingle() {
    $node = $this->getNode();
    $boolean_single = $node->get('field_boolean_single')->getString() === '1' ? TRUE : FALSE;

    $this->assertSame($boolean_single, using($node)->value('field_boolean_single'));
  }

  /**
   * Test field_color_single.
   */
  public function testFieldColorSingle() {
    $node = $this->getNode();
    $color = $node->get('field_color_single')->getValue()[0]['color'];
    $opacity = (float) $node->get('field_color_single')->getValue()[0]['opacity'];

    $this->assertSame($color, using($node)->value('field_color_single.color'));
    $this->assertSame($opacity, using($node)->value('field_color_single.opacity'));
  }

  /**
   * Test field_color_multi.
   */
  public function testFieldColorMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_color_multi')->toArray();
    $drupal_value = $node->get('field_color_multi')->getValue();

    $this->assertSame($drupal_value, $fluent_collection);

    for ($key = 0; $key < count($fluent_collection); $key++) {
      $color = $node->get('field_color_multi')->getValue()[$key]['color'];
      $opacity = (float) $node->get('field_color_multi')->getValue()[$key]['opacity'];

      $this->assertSame($color, using($node)->values('field_color_multi.color')[$key]);
      $this->assertSame($opacity, using($node)->values('field_color_multi.opacity')[$key]);
    }
  }

  /**
   * Test field_date_time_single.
   */
  public function testFieldDateTimeSingle() {
    $node = $this->getNode();
    $date_fields = [
      'date' => 'field_date_date_single',
      'date_time' => 'field_date_date_time_single',
      'start_date' => 'field_date_range_all_day_single.value',
      'end_date' => 'field_date_range_all_day_single.end_value',
    ];
    $date_range_fields = [
      'field_date_range_all_day_single',
      'field_date_range_date_single',
      'field_date_range_datetime_single',
    ];

    foreach ($date_fields as $key => $field) {
      $fluent_value = using($node)->value($field);

      if ($key === 'date' || $key === 'date_time') {
        $drupal_value = $node->get($field)->date;
      }
      elseif ($key === 'start_date' || $key === 'end_date') {
        $drupal_value = $node->get($date_range_fields[0])->$key;
      }

      // Is DrupalDateTime instance.
      $this->assertInstanceOf(DrupalDateTime::class, $fluent_value);
      // Value comparation.
      $this->assertEquals($drupal_value, $fluent_value);
    }

    foreach ($date_range_fields as $field) {
      $fluent_value = using($node)->value($field);
      $drupal_value = [
        'start_date' => $node->get($field)->start_date,
        'end_date' => $node->get($field)->end_date,
      ];

      // Is Array.
      $this->assertIsArray($fluent_value);
      // Value comparation.
      $this->assertSame($drupal_value, $fluent_value);
    }
  }

  /**
   * Test field_date_time_multi.
   */
  public function testFieldDateTimeMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values([
      'field_date_date_multi',
      'field_date_date_time_multi',
      'field_date_range_all_day_multi',
      'field_date_range_date_multi',
      'field_date_range_datetime_multi',
    ]);

    foreach ($fluent_collection as $field => $array_values) {
      foreach ($array_values as $key => $value) {
        if (!is_array($value)) {
          // Date fields.
          $drupal_value = $node->get($field)[$key]->date;
        }
        else {
          // Date range fields.
          $drupal_value = [
            'start_date' => $node->get($field)[$key]->start_date,
            'end_date' => $node->get($field)[$key]->end_date,
          ];
        }

        // Value comparation.
        $this->assertSame($drupal_value, $value);
      }
    }
  }

  /**
   * Test field_email_single.
   */
  public function testFieldEmailSingle() {
    $node = $this->getNode();
    $drupal_value = $node->get('field_email_single')->getString();
    $fluent_value = using($node)->value('field_email_single');

    $this->assertSame($drupal_value, $fluent_value);
  }

  /**
   * Test field_email_multi.
   */
  public function testFieldEmailMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_email_multi');

    foreach ($fluent_collection as $key => $value) {
      $this->assertSame($node->get('field_email_multi')->getValue()[$key]['value'], $value);
    }
  }

  /**
   * Test field_link_single.
   */
  public function testFieldLinkSingle() {
    $node = $this->getNode();
    $title = $node->get('field_link_single')->getValue()[0]["title"];
    $uri = $node->get('field_link_single')->getValue()[0]["uri"];
    $options = $node->get('field_link_single')[0]->getUrl()->getOptions();

    $this->assertSame($title, using($node)->value('field_link_single.title'));
    $this->assertSame($uri, using($node)->value('field_link_single')->getUri());
    $this->assertSame($options, using($node)->value('field_link_single')->getOptions());
  }

  /**
   * Test field_link_multi.
   */
  public function testFieldLinkMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_link_multi');

    foreach ($fluent_collection as $key => $fluent_value) {
      $title = $node->get('field_link_multi')->getValue()[$key]["title"];
      $uri = $node->get('field_link_multi')->getValue()[$key]["uri"];
      $options = $node->get('field_link_multi')[$key]->getUrl()->getOptions();

      $this->assertSame($title, using($node)->values('field_link_multi.title')[$key]);
      $this->assertSame($uri, $fluent_value->getUri());
      $this->assertSame($options, $fluent_value->getOptions());
    }
  }

  /**
   * Test field_timestamp_single.
   */
  public function testFieldTimestampSingle() {
    $node = $this->getNode();
    $drupal_value = DrupalDateTime::createFromTimestamp(
      (int) $node->get('field_timestamp_single')->getString()
    );

    $this->assertEquals($drupal_value, using($node)->value('field_timestamp_single'));
  }

  /**
   * Test field_timestamp_multi.
   */
  public function testFieldTimestampMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_timestamp_multi');

    foreach ($fluent_collection as $key => $fluent_value) {
      $drupal_value = DrupalDateTime::createFromTimestamp(
        (int) $node->get('field_timestamp_multi')[$key]->getString()
      );

      $this->assertEquals($drupal_value, $fluent_value);
    }
  }

  /**
   * Test field_number_decimal_single.
   */
  public function testFieldNumberDecimalSingle() {
    $node = $this->getNode();
    $drupal_value = (float) $node->get('field_number_decimal_single')->getString();

    $this->assertSame($drupal_value, using($node)->value('field_number_decimal_single'));
  }

  /**
   * Test field_number_decimal_multi.
   */
  public function testFieldNumberDecimalMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_number_decimal_multi');

    foreach ($fluent_collection as $key => $fluent_value) {
      $drupal_value = (float) $node->get('field_number_decimal_multi')[$key]->getString();

      $this->assertSame($drupal_value, $fluent_value);
    }
  }

  /**
   * Test field_number_float_single.
   */
  public function testFieldNumberFloatSingle() {
    $node = $this->getNode();
    $drupal_value = (float) $node->get('field_number_float_single')->getString();

    $this->assertSame($drupal_value, using($node)->value('field_number_float_single'));
  }

  /**
   * Test field_number_float_multi.
   */
  public function testFieldNumberFloatMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_number_float_multi');

    foreach ($fluent_collection as $key => $fluent_value) {
      $drupal_value = (float) $node->get('field_number_float_multi')[$key]->getString();

      $this->assertSame($drupal_value, $fluent_value);
    }
  }

  /**
   * Test field_number_int_single.
   */
  public function testFieldNumberIntSingle() {
    $node = $this->getNode();
    $drupal_value = (int) $node->get('field_number_int_single')->getString();

    $this->assertSame($drupal_value, using($node)->value('field_number_int_single'));
  }

  /**
   * Test field_number_int_multi.
   */
  public function testFieldNumberIntMulti() {
    $node = $this->getNode();
    $fluent_collection = using($node)->values('field_number_int_multi');

    foreach ($fluent_collection as $key => $fluent_value) {
      $drupal_value = (int) $node->get('field_number_int_multi')[$key]->getString();

      $this->assertSame($drupal_value, $fluent_value);
    }
  }

}
