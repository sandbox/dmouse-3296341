<?php

namespace Drupal\Tests\fluent_demo\Functional;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\BrowserTestBase;

/**
 * Base functions for Fluent testing.
 *
 * @group fluent
 */
abstract class FluentTestBase extends BrowserTestBase {

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Modules for core functionality.
    'media',
    'node',
    'block_content',
    'comment',
    'datetime',
    'paragraphs',
    'link',

    // This custom module.
    'fluent_test',
    'fluent',
  ];

  /**
   * Generate media.
   */
  protected function getMediaImage(): Media {
    $media = Media::create([
      'bundle' => 'image',
      'uid' => \Drupal::currentUser()->id(),
      'field_media_image' => [
        'target_id' => $this->getFileImage()->id(),
      ],
    ]);

    $media->save();
    return $media;
  }

  /**
   * Generate Taxonomy.
   */
  public function getTaxonomyTerm(): Term {
    return Term::create([
      'name' => $this->randomGenerator->string(),
      'vid' => 'tags',
    ]);
  }

  /**
   * Generate File.
   */
  protected function getFileImage(): File {
    $file_data = file_get_contents(
      $this->randomGenerator->image('/tmp/rand.jpg', '200x200', '500x500')
    );
    $fileRepo = \Drupal::service('file.repository');

    return $fileRepo->writeData($file_data, 'public://' . $this->randomGenerator->string() . '/');
  }

  /**
   * Generate Number.
   */
  protected function getNumber($min = 0, $max = 1, $n = 2) {
    return strval(round($min + lcg_value() * (abs($max - $min)), $n));
  }

  /**
   * Generate date.
   */
  protected function getDate($format, $time) {
    if ($format === 'date') {
      $currentDate = date('Y-m-d', $time);
    }
    elseif ($format === 'date_time') {
      $currentDate = date('Y-m-d', time()) . 'T' . date('H:i:s', $time);
    }

    return $currentDate;
  }

  /**
   * Generate timestamp.
   */
  public function getTimeStamp() {
    return strval(mt_rand(0, time()));
  }

  /**
   * Generate node with fields.
   */
  protected function getNode(): Node {
    $node = Node::create([
      'type' => 'content_fields',
      'title' => $this->randomGenerator->string(),
      'field_boolean_single' => strval(rand(0, 1)),
      'field_color_single' => [
        'color' => '#F48668',
        'opacity' => $this->getNumber(),
      ],
      'field_color_multi' => [
        0 => ['color' => '#F7B32B', 'opacity' => $this->getNumber()],
        1 => ['color' => '#AB87FF', 'opacity' => $this->getNumber()],
      ],
      'field_date_date_single' => ["value" => $this->getDate('date', time())],
      'field_date_date_time_single' => ["value" => $this->getDate('date_time', time())],
      'field_date_range_all_day_single' => [
        "value" => $this->getDate('date_time', time()),
        "end_value" => $this->getDate('date_time', time() + 1),
      ],
      'field_date_range_date_single' => [
        "value" => $this->getDate('date', time()),
        "end_value" => $this->getDate('date', time() + 86400),
      ],
      'field_date_range_datetime_single' => [
        "value" => $this->getDate('date_time', time()),
        "end_value" => $this->getDate('date_time', time() + 1),
      ],
      'field_date_date_multi' => [
        ["value" => $this->getDate('date', time())],
        ["value" => $this->getDate('date', time() + 86400)],
        ["value" => $this->getDate('date', time() + 172800)],
      ],
      'field_date_date_time_multi' => [
        ["value" => $this->getDate('date_time', time())],
        ["value" => $this->getDate('date_time', time() + 1)],
        ["value" => $this->getDate('date_time', time() + 2)],
      ],
      'field_date_range_all_day_multi' => [
        [
          "value" => $this->getDate('date_time', time()),
          "end_value" => $this->getDate('date_time', time() + 1),
        ],
        [
          "value" => $this->getDate('date_time', time()),
          "end_value" => $this->getDate('date_time', time() + 2),
        ],
        [
          "value" => $this->getDate('date_time', time()),
          "end_value" => $this->getDate('date_time', time() + 3),
        ],
      ],
      'field_date_range_date_multi' => [
        [
          "value" => $this->getDate('date', time()),
          "end_value" => $this->getDate('date', time() + 86400),
        ],
        [
          "value" => $this->getDate('date', time()),
          "end_value" => $this->getDate('date', time() + 172800),
        ],
        [
          "value" => $this->getDate('date', time()),
          "end_value" => $this->getDate('date', time() + 345600),
        ],
      ],
      'field_date_range_datetime_multi' => [
        [
          "value" => $this->getDate('date_time', time()),
          "end_value" => $this->getDate('date_time', time() + 1),
        ],
        [
          "value" => $this->getDate('date_time', time()),
          "end_value" => $this->getDate('date_time', time() + 2),
        ],
        [
          "value" => $this->getDate('date_time', time()),
          "end_value" => $this->getDate('date_time', time() + 3),
        ],
      ],
      'field_email_single' => ['value' => 'sample@sample.com'],
      'field_email_multi' => [
        ['value' => 'email1@gmail.com'],
        ['value' => 'email2@gmail.com'],
        ['value' => 'email3@gmail.com'],
      ],
      'field_link_single' => [
        'uri' => "https://git.drupalcode.org/project/fluent",
        'title' => "fluent",
        'options' => [],
      ],
      'field_link_multi' => [
        [
          'uri' => "https://git.drupalcode.org/project/fluent",
          'title' => "fluent",
          'options' => [],
        ],
        [
          'uri' => "https://indava.dev/indava/fluent-test",
          'title' => "fluent-test",
          'options' => [],
        ],
        [
          'uri' => "https://www.drupal.org/project/fluent",
          'title' => "fluent-module",
          'options' => [],
        ],
      ],
      'field_timestamp_single' => ['value' => $this->getTimeStamp()],
      'field_timestamp_multi' => [
        ['value' => $this->getTimeStamp()],
        ['value' => $this->getTimeStamp()],
        ['value' => $this->getTimeStamp()],
      ],
      'field_list_float_single' => ['value' => $this->getNumber(0.1, 0.3, 1)],
      'field_list_int_single' => ['value' => $this->getNumber(1, 3, 0)],
      'field_number_decimal_single' => ['value' => $this->getNumber(-10, 10, 2)],
      'field_number_decimal_multi' => [
        ['value' => $this->getNumber(-10, 10, 2)],
        ['value' => $this->getNumber(-10, 10, 2)],
        ['value' => $this->getNumber(-10, 10, 2)],
      ],
      'field_number_float_single' => ['value' => $this->getNumber(-10, 10, 1)],
      'field_number_float_multi' => [
        ['value' => $this->getNumber(-10, 10, 1)],
        ['value' => $this->getNumber(-10, 10, 1)],
        ['value' => $this->getNumber(-10, 10, 1)],
      ],
      'field_number_int_single' => ['value' => $this->getNumber(-10, 10, 0)],
      'field_number_int_multi' => [
        ['value' => $this->getNumber(-10, 10, 0)],
        ['value' => $this->getNumber(-10, 10, 0)],
        ['value' => $this->getNumber(-10, 10, 0)],
      ],
    ]);

    $node->save();
    return $node;
  }

}
